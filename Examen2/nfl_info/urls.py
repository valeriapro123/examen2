from django.urls import path
from . import views

urlpatterns = [
    path('equipos/', views.lista_equipos, name='lista_equipos'),
    path('ciudades/', views.lista_ciudades, name='lista_ciudades'),
    path('estadios/', views.lista_estadios, name='lista_estadios'),
]


urlpatterns = [
    # Otras rutas
    path('equipos/crear/', views.crear_equipo, name='crear_equipo'),
    path('equipos/editar/<int:equipo_id>/', views.editar_equipo, name='editar_equipo'),
    path('equipos/eliminar/<int:equipo_id>/', views.eliminar_equipo, name='eliminar_equipo'),
]


urlpatterns = [
    # Otras rutas
    path('ciudades/crear/', views.crear_ciudad, name='crear_ciudad'),
    path('ciudades/editar/<int:ciudad_id>/', views.editar_ciudad, name='editar_ciudad'),
    path('ciudades/eliminar/<int:ciudad_id>/', views.eliminar_ciudad, name='eliminar_ciudad'),
]


urlpatterns = [
    # Otras rutas
    path('estadios/crear/', views.crear_estadio, name='crear_estadio'),
    path('estadios/editar/<int:estadio_id>/', views.editar_estadio, name='editar_estadio'),
    path('estadios/eliminar/<int:estadio_id>/', views.eliminar_estadio, name='eliminar_estadio'),
]