from django.shortcuts import render, redirect
from .models import Equipo, Ciudad, Estadio
from .forms import EquipoForm, estadioforms  # crear formulario para la entrada de datos
from .forms import CiudadForm  # crear formulario para la entrada de datos


def lista_equipos(request):
    equipos = Equipo.objects.all()
    return render(request, 'nfl_app/lista_equipos.html', {'equipos': equipos})

def lista_ciudades(request):
    ciudades = Ciudad.objects.all()
    return render(request, 'nfl_app/lista_ciudades.html', {'ciudades': ciudades})

def lista_estadios(request):
    estadios = Estadio.objects.all()
    return render(request, 'nfl_app/lista_estadios.html', {'estadios': estadios})



def crear_equipo(request):
    if request.method == 'post':
        form = EquipoForm(request.post)
        if form.is_valid():
            form.save()
            return redirect('lista_equipos')
    else:
        form = EquipoForm()
    return render(request, 'nfl_app/crear_equipo.html', {'form': form})

def editar_equipo(request, equipo_id):
    equipo = Equipo.objects.get(pk=equipo_id)
    if request.method == 'post':
        form = EquipoForm(request.post, instance=equipo)
        if form.is_valid():
            form.save()
            return redirect('lista_equipos')
    else:
        form = EquipoForm(instance=equipo)
    return render(request, 'nfl_app/editar_equipo.html', {'form': form, 'equipo': equipo})

def eliminar_equipo(request, equipo_id):
    equipo = Equipo.objects.get(pk=equipo_id)
    if request.method == 'post':
        equipo.delete()
        return redirect('lista_equipos')
    return render(request, 'nfl_app/eliminar_equipo.html', {'equipo': equipo})




def crear_ciudad(request):
    if request.method == 'post':
        form = CiudadForm(request.post)
        if form.is_valid():
            form.save()
            return redirect('lista_ciudades')
    else:
        form = CiudadForm()
    return render(request, 'nfl_app/crear_ciudad.html', {'form': form})

def editar_ciudad(request, ciudad_id):
    ciudad = Ciudad.objects.get(pk=ciudad_id)
    if request.method == 'post':
        form = CiudadForm(request.post, instance=ciudad)
        if form.is_valid():
            form.save()
            return redirect('lista_ciudades')
    else:
        form = CiudadForm(instance=ciudad)
    return render(request, 'nfl_app/editar_ciudad.html', {'form': form, 'ciudad': ciudad})

def eliminar_ciudad(request, ciudad_id):
    ciudad = Ciudad.objects.get(pk=ciudad_id)
    if request.method == 'post':
        ciudad.delete()
        return redirect('lista_ciudades')
    return render(request, 'nfl_app/eliminar_ciudad.html', {'ciudad': ciudad})


def crear_estadio(request):
    if request.method == 'post':
        form = estadioforms(request.post)
        if form.is_valid():
            form.save()
            return redirect('lista_estadios')
    else:
        form = estadioforms()
    return render(request, 'nfl_app/crear_estadio.html', {'form': form})

def editar_estadio(request, estadio_id):
    estadio = Estadio.objects.get(pk=estadio_id)
    if request.method == 'post':
        form = estadioforms(request.post, instance=estadio)
        if form.is_valid():
            form.save()
            return redirect('lista_estadios')
    else:
        form = estadioforms(instance=estadio)
    return render(request, 'nfl_app/editar_estadio.html', {'form': form, 'estadio': estadio})

def eliminar_estadio(request, estadio_id):
    estadio = Estadio.objects.get(pk=estadio_id)
    if request.method == 'post':
        estadio.delete()
        return redirect('lista_estadios')
    return render(request, 'nfl_app/eliminar_estadio.html', {'estadio': estadio})

