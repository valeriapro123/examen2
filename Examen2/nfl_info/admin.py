from django.contrib import admin
from .models import *

# Register your models here.
@admin.register(Estadio)
class estAdmin(admin.ModelAdmin):
    list_display = [
        "Nombre",
        "Equipos",
    ]

@admin.register(Equipo)
class EquiAdmin(admin.ModelAdmin):
    list_display = [
        "Nombre",
        "Ciudad",
    ]

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    list_display = [
        "Nombre",
    ]