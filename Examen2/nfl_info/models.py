from django.db import models


class Ciudad(models.Model):
    nombre = models.CharField(max_length=50)

class Equipo(models.Model):
    nombre = models.CharField(max_length=50)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)

class Estadio(models.Model):
    nombre = models.CharField(max_length=50)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)