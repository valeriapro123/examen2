from django import forms
from .models import *

##estadios

class estadioforms(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Equipos": forms.Select(attrs={"class":"form-select form-control"}),
            }



class ediarestadioforms(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Equipos": forms.Select(attrs={"class":"form-select form-control"}),
            }

##equpos

class equipoforms(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Ciudad": forms.Select(attrs={"class":"form-select form-control"}),
            }
        
class editarequipoforms(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Ciudad": forms.Select(attrs={"class":"form-select form-control"}),
            }

##ciudad

class ciudadforms(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }

class editarciudadforms(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }
        
        